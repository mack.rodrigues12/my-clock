import React, { useState, useEffect } from 'react';
import "../../style/Stopwatch.css";

const Stopwatch = () => {
    const [time, setTime] = useState(0);
    const [timerOn, setTimerOn] = useState(false);

    useEffect(() => {
        let interval = null;
    
        if (timerOn) {
          interval = setInterval(() => {
            setTime((prevTime) => prevTime + 10);
          }, 10);
        } else {
          clearInterval(interval);
        }
    
        return () => clearInterval(interval);
      }, [timerOn]);
    return (
        <div className="stopwatch">
        <h2> 
            <span>{("0" + Math.floor((time / 60000) % 60)).slice(-2)}:</span>
            <span>{("0" + Math.floor((time / 1000) % 60)).slice(-2)}:</span>
            <span>{("0" + ((time / 10) % 100)).slice(-2)}</span>
        </h2>
        <div className="buttonsStopwatch">
            {!timerOn && time === 0 && (
            <button className="stopwatch-btn stopwatch-btn-gre" onClick={() => setTimerOn(true)}>Start</button>
            )}
            {timerOn && <button className="stopwatch-btn stopwatch-btn-red" onClick={() => setTimerOn(false)}>Stop</button>}
            {!timerOn && time > 0 && (
            <button className="stopwatch-btn stopwatch-btn-yel" onClick={() => setTime(0)}>Reset</button>
            )}
            {!timerOn && time > 0 && (
            <button className="stopwatch-btn stopwatch-btn-gre" onClick={() => setTimerOn(true)}>Resume</button>
            )}
            </div>
        </div>
    )
}

export default Stopwatch
