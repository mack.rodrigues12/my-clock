import React, { useState } from 'react';
import "../../style/Clock.css";

const Clock = () => {
    let time = new Date().toLocaleTimeString();

    const [cTime, setCTime] = useState(time);

    const updateTime = () => {
        time = new Date().toLocaleTimeString();
        setCTime(time);
    }

    setInterval(updateTime,1000);

    return(
        <>
        <h1><span className="center">{time}</span></h1>
        </>
    );
}
export default Clock;