import React, { useState, useEffect } from 'react';
import Clock from '../src/Components/Clock/Clock';
import Countdown from '../src/Components/Countdown-Timer/Countdown';
import Stopwatch from '../src/Components/Stopwatch/Stopwatch';

function App() {
  const [time, setTime] = useState("");
  const[timerDays, setTimerDays] = useState();
  const[timerHours, setTimerHours] = useState();
  const[timerMinutes, setTimerMinutes] = useState();
  const[timerSeconds, setTimerSeconds] = useState();

  let interval;

  const startTimer = () =>{
    const countDownDate = new Date ("November 05, 2021").getTime();

    interval = setInterval(() =>{
      const now = new Date().getTime();

      const distance = countDownDate - now;

      const days = Math.floor(distance/(24*60*60*1000));
      const hours = Math.floor(distance%(24*60*60*1000)/(1000*60*60));
      const minutes = Math.floor(distance%(60*60*1000)/(1000*60));
      const seconds = Math.floor(distance%(60*1000)/(1000));

      if(distance<0){
        //stop Timer 

        clearInterval(interval.current);
        setTime("COUNTDOWN FINISHED");
      }else{
        //Update Timer
        setTimerDays(days);
        setTimerHours(hours);
        setTimerMinutes(minutes);
        setTimerSeconds(seconds);
      }
    });
  }

  useEffect(()=>{
    startTimer();
  })

  return (
    <div className="App">
    <Clock />
    <Countdown timerDays={timerDays} timerHours={timerHours} timerMinutes={timerMinutes} timerSeconds={timerSeconds}/>
    <Stopwatch/>
   </div>
  );
}

export default App;
